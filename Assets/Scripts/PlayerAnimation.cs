﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public GameObject wingLeft, wingRight;
    public GameObject tail;

    public void Jump()
    {
        iTween.PunchRotation(wingLeft, new Vector3(90, -40, 0), 0.6f);
        iTween.PunchRotation(wingRight, new Vector3(90, -40, 0), 0.6f);
        iTween.PunchRotation(tail, new Vector3(0, 0, 100), 0.6f);
        Invoke("ResetRot", 0.6f);
    }

    void ResetRot()
    {
        wingLeft.transform.localRotation = Quaternion.Euler(0, 0, 0);
        wingRight.transform.localRotation = Quaternion.Euler(0, 0, 0);
        tail.transform.localRotation = Quaternion.Euler(0, 0, 0);
    }
}
