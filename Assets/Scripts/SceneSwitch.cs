﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : MonoBehaviour
{
    public void ChangeScene(int sceneNumber = 0)
    {
        SceneManager.LoadScene(sceneNumber);
    }

    public void OnTriggerEnter(Collider other)
    {
        ChangeScene(1);
    }
}
