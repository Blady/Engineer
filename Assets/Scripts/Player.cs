﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    float newX;
    float speed = 6.2f; // old == 0.2f
    Rigidbody _rb;
    RaycastHit hit;
    bool canJump = true;
    bool flipped = false;

    public static Player player;
    CharacterController controller;
    Vector3 moveDirection;

    Quaternion previousRotation = new Quaternion();
    PlayerAnimation ani;

    private void Awake()
    {
        player = this;
        controller = GetComponent<CharacterController>();
        moveDirection = new Vector3();
        ani = GetComponent<PlayerAnimation>();
    }

    void Start ()
    {
        _rb = GetComponent<Rigidbody>();
	}
	
	void Update ()
    {
        CheckForQuit();
        Walk();
        Jump();
        CheckRotation();
        if (flipped)
        {
            transform.Rotate(new Vector3(0, 180, 0));
        }
        //_rb.velocity.Normalize();
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection.y -= 9.81f * Time.deltaTime;
        if (moveDirection.y < -5)
            moveDirection.y = -5;
        controller.Move(moveDirection * Time.deltaTime);
    }

    void Walk()
    {
        newX = Input.GetAxis("Horizontal");
        newX *= speed;
        if (!IsGrounded())
            newX *= 0.5f;
        if ((newX > 0.1f && flipped) || (newX < -0.1f && !flipped))
        {
            Flip();
        }
        if (flipped)
            newX *= -1;
        moveDirection.x = newX;
        moveDirection.z = 0;
        
        //transform.Translate(new Vector3(newX, 0, 0));
        //_rb.velocity += new Vector3(newX, 0, 0);
        //_rb.AddForce(new Vector3(newX, 0, 0), ForceMode.Impulse);


    }

    void Jump()
    {
        if(Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.Joystick1Button0) )
        {
            if (IsGrounded())
            {
                //_rb.AddForce(new Vector3(0, 65, 0), ForceMode.Impulse);
                moveDirection.y = 4;
                canJump = false;
                Invoke("ResetJump", 0.3f);
                ani.Jump();
            }
        }
    }

    bool IsGrounded()
    {
        bool grounded = false;

        if (Physics.Raycast(transform.position, transform.up * -1, out hit, 0.6f))
        {
            if(hit.transform.gameObject.tag == "Floor")
            {
                grounded = true;
            }
        }
        return grounded;
    }

    void CheckRotation()
    {
        if (Physics.Raycast(transform.position, transform.up * -1, out hit, 0.8f))
        {
            if (hit.transform.gameObject.tag == "Floor")
            {
                transform.rotation = hit.transform.rotation;
                previousRotation = transform.rotation;
            }
        }
        else
        {
            transform.rotation = previousRotation;
        }
    }

    void ResetJump()
    {
        canJump = true;
    }

    void Flip()
    {
        flipped = !flipped;
        Camera.main.transform.RotateAround(transform.position, transform.up, 180);
        //Throwing.throwing.basePoint.localPosition = new Vector3(Throwing.throwing.basePoint.transform.localPosition.x * -1, 0.31f,0);
        //if(flipped)
        //    Throwing.throwing.basePoint.localRotation = Quaternion.Euler(0, 180, 0);
        //else
        //    Throwing.throwing.basePoint.localRotation = Quaternion.Euler(0, 0, 0);
    }

    public static float Remap(float value, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (value - oldMin) / (oldMax - oldMin) * (newMax - newMin) + newMin;
    }

    void CheckForQuit()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
