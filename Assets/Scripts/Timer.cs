﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    Text text;
    float time = 0;

	void Start ()
    {
        text = GetComponent<Text>();
	}
	
	void Update ()
    {
        time += Time.deltaTime;
        text.text = Mathf.RoundToInt(time).ToString();
	}
}
