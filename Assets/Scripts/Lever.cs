﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : TriggerObject
{
    public Reactor reactor;
    public GameObject switcher;

    void Start()
    {
        reactor.allTriggers.Add(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wrench" && !Throwing.throwing.isHolding)
        {
            Collider[] allCol = GetComponents<Collider>();
            foreach (Collider temp in allCol)
                temp.enabled = false;
            reactor.allTriggers.Remove(this);
            if (reactor.allTriggers.Count <= 0)
            {
                reactor.OnActivate();
            }
            switcher.transform.localRotation = Quaternion.Euler(0, 0, 40);
        }
    }
}
