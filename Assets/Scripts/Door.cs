﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Reactor
{
    public override void OnActivate()
    {
        base.OnActivate();
        gameObject.SetActive(false);
    }
}
