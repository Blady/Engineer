﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Throwing : MonoBehaviour
{
    public bool controller = true;

    public Transform basePoint;
    public Transform hand;
    public Wrench wrench;
    public Ray ray;
    public float maxUpAngle, maxDownAngle;
    public float maxThrowPower, throwPowerIncreaseRate;
    float throwPowerIncrease;
    float currentThrowPower = 0;
    public Image meter;
    public GameObject throwMeter;

    public bool isHolding = false;
    public bool isThrowing = false;

    public static Throwing throwing;

    private void Awake()
    {
        throwing = this;
        throwPowerIncrease = maxThrowPower / 100 * throwPowerIncreaseRate;
    }

    void Start ()
    {
        wrench.Pickup();
	}
	
	void Update ()
    {
        Rotation();
        CheckThrow();
    }

    void Rotation()
    {
        if (controller)
            basePoint.Rotate(new Vector3(0, 0, Input.GetAxis("Vertical")));
        else
        {
            if (Input.GetKey(KeyCode.Q))
            {
                basePoint.Rotate(new Vector3(0, 0, 1));
            }
            else if (Input.GetKey(KeyCode.E))
            {
                basePoint.Rotate(new Vector3(0, 0, -1));
            }
        }
        if (basePoint.rotation.eulerAngles.z > maxUpAngle)
            basePoint.localRotation = Quaternion.Euler(new Vector3(0, 0, maxUpAngle));
        else if (basePoint.rotation.eulerAngles.z < maxDownAngle)
            basePoint.localRotation = Quaternion.Euler(new Vector3(0, 0, maxDownAngle));
    }

    void CheckThrow()
    {
        if (!isHolding)
            return;
        if (Input.GetKey(KeyCode.F) || Input.GetKey(KeyCode.Joystick1Button1))
        {
            isThrowing = true;
            currentThrowPower += throwPowerIncrease;
        }
        if ((Input.GetKeyUp(KeyCode.F) || Input.GetKeyUp(KeyCode.Joystick1Button1)) && isThrowing || currentThrowPower >= maxThrowPower)
        {
            isThrowing = false;
            Throw();
            currentThrowPower = 0;
        }
        throwMeter.SetActive(isThrowing);
        meter.fillAmount = Player.Remap(currentThrowPower, 0, maxThrowPower, 0, 1);
    }

    void Throw()
    {
        Vector3 dir = basePoint.transform.right;
        dir.Normalize();
        wrench.Throw(dir, currentThrowPower);
        //wrench.Throw(new Vector3(basePoint.rotation.eulerAngles.z, 0, 0), currentThrowPower);
        isHolding = false;
    }
}
