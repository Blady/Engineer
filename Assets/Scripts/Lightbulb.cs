﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightbulb : TriggerObject
{
    public Reactor reactor;
    public Reactor extraReactor = null;

	void Start ()
    {
        reactor.allTriggers.Add(this);
	}
	
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Wrench")
        {
            GetComponent<Collider>().enabled = false;
            reactor.allTriggers.Remove(this);
            if (reactor.allTriggers.Count <= 0)
            {
                reactor.OnActivate();
            }
            if(extraReactor != null)
            {
                extraReactor.allTriggers.Remove(this);
                if (extraReactor.allTriggers.Count <= 0)
                {
                    extraReactor.OnActivate();
                }
            }
            gameObject.SetActive(false);
        }
    }
}
