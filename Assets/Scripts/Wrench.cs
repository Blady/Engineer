﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wrench : MonoBehaviour
{
    Rigidbody _rb;
    BoxCollider _col;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        BoxCollider[] allCol = GetComponents<BoxCollider>();
        foreach (BoxCollider temp in allCol)
        {
            if (!temp.isTrigger)
            {
                _col = GetComponent<BoxCollider>();
            }
        }
    }

    void Start ()
    {
        Throwing.throwing.wrench = this;
	}

    void Update()
    {
        if (Throwing.throwing.isHolding)
        {
            transform.position = Throwing.throwing.hand.transform.position;
            transform.rotation = Throwing.throwing.basePoint.transform.rotation;
        }
    }

    public void Pickup()
    {
        _rb.isKinematic = true;
        Throwing.throwing.isHolding = true;
        _col.enabled = false;
    }

    public void Throw(Vector3 direction, float power)
    {
        _rb.isKinematic = false;
        _rb.AddForce(direction * power, ForceMode.Impulse);
        _col.enabled = true;
    }

    private void OnTriggerEnter (Collider other)
    {
        if (Throwing.throwing.isHolding)
            return;
        if(other.gameObject.tag == "Player")
        {
            Pickup();
        }
    }
}
