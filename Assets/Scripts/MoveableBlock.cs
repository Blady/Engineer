﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableBlock : MonoBehaviour
{
    public Transform point1, point2;
    bool toPoint2 = true;
    public float timeToReach;
    Transform target;

	void Start ()
    {
        Arived();
	}
	
	void Update ()
    {
		if(Vector3.Distance(transform.position, target.position) < 0.1f)
        {
            Arived();
        }
	}

    void Arived()
    {
        iTween.Stop(gameObject);
        toPoint2 = !toPoint2;
        if (toPoint2)
        {
            target = point2;
        }
        else
        {
            target = point1;
        }
        iTween.MoveTo(gameObject, iTween.Hash(
                       "position", target.position,
                       "time", timeToReach,
                       "easeType", iTween.EaseType.linear));
    }
}
